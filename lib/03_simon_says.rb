def echo(str)
  return str
end

def shout(str)
  str.upcase
end

def repeat(str, num = 2)
  result = []
  num.times do
    result << str
  end
  result.join(" ")
end

def start_of_word(str, length)
  str[0...length]
end

def first_word(sentence)
  sentence.split(" ")[0]
end

def titleize(sentence)
  words = sentence.split(" ")
  result = []
  little_words = ["a", "the", "but", "or", "over", "up", "by", "and"]
  words.each_with_index do |word, index|
    if little_words.include?(word) && index != 0
      result[index] = word.downcase
    else
      result[index] = word[0].upcase + word[1..-1]
    end
  end
  result.join(" ")
end
