def add(num_one, num_two)
  num_one + num_two
end

def subtract(num_one, num_two)
  num_one - num_two
end

def sum(arr)
  arr.reduce(0, :+)
end

def multiply(*args)
  args.reduce(1, :*)
end

def power(num_one, num_two)
  num_one ** num_two
end

def factorial(num)
  if num == 0
    return 1
  end

  result = 1
  counter = 1
  while counter <= num
    result *= counter
    counter += 1
  end
  result
end
