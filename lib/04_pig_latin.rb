def translate(sentence)
  vowels = "aeiouAEIOU"
  words = sentence.split(" ")
  result = []
  capialized = false

  words.each_with_index do |word, index|
    capitalized = false
    if word != word.downcase
      capitalized = true
    end
    if vowels.include?(word[0])
      result[index] = word + "ay"
    else
      vowel_idx = word.split("").find_index { |ch| vowels.include?(ch) }
      if word[vowel_idx].downcase == "u" && word[vowel_idx-1].downcase == "q"
        result[index] = word[vowel_idx+1..-1] + word[0..vowel_idx] + "ay"
      else
        result[index] = word[vowel_idx..-1] + word[0...vowel_idx] + "ay"
      end
    end
    if capitalized
      result[index] = result[index][0].upcase + result[index][1..-1].downcase
    end
  end

  result.join(" ")
end
